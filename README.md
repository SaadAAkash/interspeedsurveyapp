# Interspeed Survey App 

An app for tracking and monitoring team members.

## Used Libraries in Ver01

* Gson for convert Java Objects into their JSON and vice versa
* Realm for mobile database, as a replacement for SQLite & ORMs
* Okhttp for sending, & receiving HTTP requests, it provides an implementation of the HttpUrlConnection interface
* Google play-services-location for location tracking

## Ver01 Features

* Basic login Screen with UserID, TeamID and Password (all provided as defualt) 
* A Continue button 
* Main UI contains Time & Date, Start Journey and Check In list 
* Check In shows attributes of region, area house, point, route, route id and cluster
* Only Upload from "Check In List" alerts the inavilability of data 
* MapActivity returns current posiiton correctly 

## Ver01 Backlogs

* Back button from the upper left corner of every UI screen doesn't work
* Internet connectivity manager not used for checking, so map doesn't work
* Internet connectivity manager not used for checking, so upload doesn't work
* No Intro Slider or anything illustrating the instruction of the app workflow for the user
* No logout
* The upload can be done using FAB
* Lots of UI works

## Ver01 State

![Example Image][1]

[1]: https://bytebucket.org/SaadAAkash/interspeedsurveyapp/raw/2c43e80bf5c2baa9aa7d9c85a1c5c9bb76fff80b/SurveyAppVer01SS/5.png
