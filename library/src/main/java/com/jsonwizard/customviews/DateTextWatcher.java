package com.jsonwizard.customviews;

import android.support.v7.widget.TintContextWrapper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.jsonwizard.R;
import com.jsonwizard.interfaces.JsonApi;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;

import java.util.Calendar;

/**
 * Created by android on 11/28/2017.
 */

public class DateTextWatcher implements TextWatcher {

    private View mView;
    private String mStepName;

    private String current = "";
    private String ddmmyyyy = "DDMMYYYY";
    private Calendar cal = Calendar.getInstance();

    public DateTextWatcher(String stepName, View view) {
        mView = view;
        mStepName = stepName;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!s.toString().equals(current)) {
            String clean = s.toString().replaceAll("[^\\d.]", "");
            String cleanC = current.replaceAll("[^\\d.]", "");

            int cl = clean.length();
            int sel = cl;
            for (int i = 2; i <= cl && i < 6; i += 2) {
                sel++;
            }
            //Fix for pressing delete next to a forward slash
            if (clean.equals(cleanC)) sel--;

            if (clean.length() < 8) {
                clean = clean + ddmmyyyy.substring(clean.length());
            } else {
                //This part makes sure that when we finish entering numbers
                //the date is correct, fixing it otherwise
                int day = Integer.parseInt(clean.substring(0, 2));
                int mon = Integer.parseInt(clean.substring(2, 4));
                int year = Integer.parseInt(clean.substring(4, 8));

                if (mon > 12) mon = 12;
                cal.set(Calendar.MONTH, mon - 1);
                year = (year < 1900) ? 1900 : (year > 2100) ? 2100 : year;
                cal.set(Calendar.YEAR, year);
                // ^ first set year for the line below to work correctly
                //with leap years - otherwise, date e.g. 29/02/2012
                //would be automatically corrected to 28/02/2012

                day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                clean = String.format("%02d%02d%02d", day, mon, year);
            }

            clean = String.format("%s/%s/%s", clean.substring(0, 2),
                    clean.substring(2, 4),
                    clean.substring(4, 8));

            sel = sel < 0 ? 0 : sel;
            current = clean;
            ((MaterialEditText) mView).setText(current);
            ((MaterialEditText) mView).setSelection(sel < current.length() ? sel : current.length());

            JsonApi api = null;
            if (mView.getContext() instanceof JsonApi) {
                api = (JsonApi) mView.getContext();
            } else if (mView.getContext() instanceof TintContextWrapper) {
                TintContextWrapper tintContextWrapper = (TintContextWrapper) mView.getContext();
                api = (JsonApi) tintContextWrapper.getBaseContext();
            } else {
                throw new RuntimeException("Could not fetch context");
            }

            String key = (String) mView.getTag(R.id.key);
            try {
                api.writeResult(mStepName, key, current);
            } catch (JSONException e) {
                // TODO- handle
                e.printStackTrace();
            }
        }
    }
}
