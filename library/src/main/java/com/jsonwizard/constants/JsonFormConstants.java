package com.jsonwizard.constants;

/**
 * Created by android on 11/26/17.
 */
public class JsonFormConstants {

    public static final String FIRST_STEP_NAME = "step1";
    public static final String EDIT_TEXT = "edit_text";
    public static final String CHECK_BOX = "check_box";
    public static final String RADIO_BUTTON = "radio";
    public static final String LABEL = "label";
    public static final String CHOOSE_IMAGE = "choose_image";
    public static final String OPTIONS_FIELD_NAME = "options";
    public static final String SPINNER = "spinner";
}
