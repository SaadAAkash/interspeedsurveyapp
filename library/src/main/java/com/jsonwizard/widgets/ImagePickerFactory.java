package com.jsonwizard.widgets;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jsonwizard.R;
import com.jsonwizard.demo.utils.ImageUtils;
import com.jsonwizard.demo.utils.ValidationStatus;
import com.jsonwizard.interfaces.CommonListener;
import com.jsonwizard.interfaces.FormWidgetFactory;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.jsonwizard.demo.utils.FormUtils.MATCH_PARENT;
import static com.jsonwizard.demo.utils.FormUtils.WRAP_CONTENT;
import static com.jsonwizard.demo.utils.FormUtils.dpToPixels;
import static com.jsonwizard.demo.utils.FormUtils.getLayoutParams;

/**
 * Created by android on 11/26/17.
 */
public class ImagePickerFactory implements FormWidgetFactory {

    public static ValidationStatus validate(ImageView imageView) {
        Boolean isRequired = Boolean.valueOf((boolean) imageView.getTag(R.id.v_required));
        if (!isRequired) {
            return new ValidationStatus(true, null);
        }
        Object path = imageView.getTag(R.id.imagePath);
        if (path instanceof String && !TextUtils.isEmpty((String) path)) {
            return new ValidationStatus(true, null);
        }
        return new ValidationStatus(false, (String) imageView.getTag(R.id.error));
    }

    @Override
    public List<View> getViewsFromJson(String stepName, Context context, JSONObject jsonObject, CommonListener listener) throws Exception {
        List<View> views = new ArrayList<>(1);
        ImageView imageView = new ImageView(context);
        imageView.setImageDrawable(context.getResources().getDrawable(R.mipmap.grey_bg));
        imageView.setTag(R.id.key, jsonObject.getString("key"));
        imageView.setTag(R.id.type, jsonObject.getString("type"));

        boolean isRequired = jsonObject.optBoolean("is_required");
        imageView.setTag(R.id.v_required, isRequired);
        if (isRequired) {
            imageView.setTag(R.id.error, jsonObject.optString("err_empty_msg"));
        }

        imageView.setScaleType(ImageView.ScaleType.CENTER);
        imageView.setLayoutParams(getLayoutParams(MATCH_PARENT, dpToPixels(context, 200), 0, 0, 0, (int) context
                .getResources().getDimension(R.dimen.default_bottom_margin)));
        String imagePath = jsonObject.optString("value");
        if (!TextUtils.isEmpty(imagePath)) {
            imageView.setTag(R.id.imagePath, imagePath);
            imageView.setImageBitmap(ImageUtils.loadBitmapFromFile(imagePath, ImageUtils.getDeviceWidth(context), dpToPixels(context, 200)));
        }
        views.add(imageView);
        Button uploadButton = new Button(context);
        uploadButton.setText(jsonObject.getString("uploadButtonText"));
        LinearLayout.LayoutParams params = getLayoutParams(WRAP_CONTENT, WRAP_CONTENT, 0, 0, 0, (int) context
                .getResources().getDimension(R.dimen.default_bottom_margin));
        params.gravity = Gravity.CENTER_HORIZONTAL;
        uploadButton.setLayoutParams(params);
        uploadButton.setOnClickListener(listener);
        uploadButton.setTag(R.id.key, jsonObject.getString("key"));
        uploadButton.setTag(R.id.type, jsonObject.getString("type"));
        views.add(uploadButton);
        return views;
    }
}
