package com.jsonwizard.widgets;

import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;

import com.jsonwizard.R;
import com.jsonwizard.customviews.DateTextWatcher;
import com.jsonwizard.customviews.GenericTextWatcher;
import com.jsonwizard.demo.utils.ValidationStatus;
import com.jsonwizard.interfaces.CommonListener;
import com.jsonwizard.interfaces.FormWidgetFactory;
import com.jsonwizard.validators.edittext.MaxLengthValidator;
import com.jsonwizard.validators.edittext.MaxValueValidator;
import com.jsonwizard.validators.edittext.MinLengthValidator;
import com.jsonwizard.validators.edittext.MinValueValidator;
import com.jsonwizard.validators.edittext.RegexValidator;
import com.jsonwizard.validators.edittext.RequiredValidator;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.util.ViewUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 11/27/17.
 */
public class EditTextFactory implements FormWidgetFactory {

    public static final int MIN_LENGTH = 0;
    public static final int MAX_LENGTH = 100;

    public static ValidationStatus validate(MaterialEditText editText) {
        boolean validate = editText.validate();
        if (!validate) {
            return new ValidationStatus(false, editText.getError().toString());
        }
        return new ValidationStatus(true, null);
    }

    @Override
    public List<View> getViewsFromJson(String stepName, Context context, JSONObject jsonObject, CommonListener listener) throws Exception {
        int minLength = MIN_LENGTH;
        int maxLength = MAX_LENGTH;
        List<View> views = new ArrayList<>(1);
        MaterialEditText editText = (MaterialEditText) LayoutInflater.from(context).inflate(
                R.layout.item_edit_text, null);
        editText.setHint(jsonObject.getString("hint"));
        editText.setFloatingLabelText(jsonObject.getString("hint"));
        editText.setId(ViewUtil.generateViewId());
        editText.setTag(R.id.key, jsonObject.getString("key"));
        editText.setTag(R.id.type, jsonObject.getString("type"));

        if (!TextUtils.isEmpty(jsonObject.optString("value"))) {
            editText.setText(jsonObject.optString("value"));
        }

        //add validators
        boolean isRequired = jsonObject.optBoolean("is_required");
        if (isRequired) {
            editText.addValidator(new RequiredValidator(jsonObject.optString("err_empty_msg")));
        }

        JSONObject minLengthObject = jsonObject.optJSONObject("min_length");
        if (minLengthObject != null) {
            int minLengthValue = minLengthObject.optInt("value", 0);
            editText.addValidator(new MinLengthValidator(isRequired, minLengthObject.getString("err"), minLengthValue));
        }

        JSONObject maxLengthObject = jsonObject.optJSONObject("max_length");
        if (maxLengthObject != null) {
            int maxLengthValue = maxLengthObject.optInt("value", 0);
            editText.addValidator(new MaxLengthValidator(isRequired, maxLengthObject.getString("err"), maxLengthValue));
        }

        JSONObject minValueObject = jsonObject.optJSONObject("min_value");
        if (minValueObject != null) {
            int minValue = minValueObject.optInt("value", 0);
            editText.addValidator(new MinValueValidator(isRequired, minValueObject.getString("err"),
                    minValue));
        }

        JSONObject maxValueObject = jsonObject.optJSONObject("max_value");
        if (maxValueObject != null) {
            int maxValue = maxValueObject.optInt("value", 0);
            editText.addValidator(new MaxValueValidator(isRequired, maxValueObject.getString("err"),
                    maxValue));
        }

        editText.setMaxCharacters(maxLength);
        editText.setMinCharacters(minLength);

        // edit type check
        String editType = jsonObject.optString("edit_type");
        if (!TextUtils.isEmpty(editType)) {
            if (editType.equals("email")) {
                editText.addValidator(new RegexValidator(isRequired, "Email is invalid", android.util.Patterns.EMAIL_ADDRESS.toString()));
            } else if (editType.equals("url")) {
                editText.addValidator(new RegexValidator(isRequired, "Web Url is invalid", Patterns.WEB_URL.toString()));
            } else if (editType.equals("date")) {
                editText.addValidator(new RegexValidator(isRequired, "Date is invalid", "^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/([0-9][0-9])?[0-9][0-9]$"));
                editText.addTextChangedListener(new DateTextWatcher(stepName, editText));
            } else if (editType.equals("integer_positive")) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                editText.addValidator(new RegexValidator(isRequired, "Input is invalid", "[0-9]+"));
            } else if (editType.equals("integer_negative")) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.addValidator(new RegexValidator(isRequired, "Input is invalid", "-?[1-9]\\d*|0"));
            } else if (editType.equals("float_positive")) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                editText.addValidator(new RegexValidator(isRequired, "Input is invalid", "^(0|[1-9]\\d*)?(\\.\\d+)?(?<=\\d)$"));
            } else if (editType.equals("float_negative")) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.addValidator(new RegexValidator(isRequired, "Input is invalid", "^-?(0|[1-9]\\d*)?(\\.\\d+)?(?<=\\d)$"));
            }
        }

        if (!editType.equals("date")) {
            editText.addTextChangedListener(new GenericTextWatcher(stepName, editText));
        }
        views.add(editText);
        return views;
    }

}
