package com.jsonwizard.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;

import com.jsonwizard.R;
import com.jsonwizard.constants.JsonFormConstants;
import com.jsonwizard.customviews.CheckBox;
import com.jsonwizard.interfaces.CommonListener;
import com.jsonwizard.interfaces.FormWidgetFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.jsonwizard.demo.utils.FormUtils.FONT_BOLD_PATH;
import static com.jsonwizard.demo.utils.FormUtils.FONT_REGULAR_PATH;
import static com.jsonwizard.demo.utils.FormUtils.MATCH_PARENT;
import static com.jsonwizard.demo.utils.FormUtils.WRAP_CONTENT;
import static com.jsonwizard.demo.utils.FormUtils.getLayoutParams;
import static com.jsonwizard.demo.utils.FormUtils.getTextViewWith;

/**
 * Created by android on 11/26/17.
 */
public class CheckBoxFactory implements FormWidgetFactory {
    @Override
    public List<View> getViewsFromJson(String stepName, Context context, JSONObject jsonObject, CommonListener listener) throws Exception {
        List<View> views = new ArrayList<>(1);
        views.add(getTextViewWith(context, 16, jsonObject.getString("label"), jsonObject.getString("key"),
                jsonObject.getString("type"), getLayoutParams(MATCH_PARENT, WRAP_CONTENT, 0, 0, 0, 0),
                FONT_BOLD_PATH));
        JSONArray options = jsonObject.getJSONArray(JsonFormConstants.OPTIONS_FIELD_NAME);
        for (int i = 0; i < options.length(); i++) {
            JSONObject item = options.getJSONObject(i);
            CheckBox checkBox = (CheckBox) LayoutInflater.from(context).inflate(R.layout.item_checkbox, null);
            checkBox.setText(item.getString("text"));
            checkBox.setTag(R.id.key, jsonObject.getString("key"));
            checkBox.setTag(R.id.type, jsonObject.getString("type"));
            checkBox.setTag(R.id.childKey, item.getString("key"));
            checkBox.setGravity(Gravity.CENTER_VERTICAL);
            checkBox.setTextSize(16);
            checkBox.setTypeface(Typeface.createFromAsset(context.getAssets(), FONT_REGULAR_PATH));
            checkBox.setOnCheckedChangeListener(listener);
            if (!TextUtils.isEmpty(item.optString("isChecked"))) {
                checkBox.setChecked(Boolean.valueOf(item.optString("isChecked")));
            }
            if (i == options.length() - 1) {
                checkBox.setLayoutParams(getLayoutParams(MATCH_PARENT, WRAP_CONTENT, 0, 0, 0, (int) context
                        .getResources().getDimension(R.dimen.extra_bottom_margin)));
            }
            views.add(checkBox);
        }
        return views;
    }
}
