package com.jsonwizard.widgets;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.jsonwizard.R;
import com.jsonwizard.interfaces.CommonListener;
import com.jsonwizard.interfaces.FormWidgetFactory;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.jsonwizard.demo.utils.FormUtils.FONT_BOLD_PATH;
import static com.jsonwizard.demo.utils.FormUtils.WRAP_CONTENT;
import static com.jsonwizard.demo.utils.FormUtils.getLayoutParams;
import static com.jsonwizard.demo.utils.FormUtils.getTextViewWith;

/**
 * Created by android on 11/27/17.
 */
public class LabelFactory implements FormWidgetFactory {
    @Override
    public List<View> getViewsFromJson(String stepName, Context context, JSONObject jsonObject, CommonListener listener) throws Exception {
        List<View> views = new ArrayList<>(1);
        LinearLayout.LayoutParams layoutParams = getLayoutParams(WRAP_CONTENT, WRAP_CONTENT, 0, 0, 0, (int) context
                .getResources().getDimension(R.dimen.default_bottom_margin));
        views.add(getTextViewWith(context, 16, jsonObject.getString("text"), jsonObject.getString("key"),
                jsonObject.getString("type"), layoutParams, FONT_BOLD_PATH));
        return views;
    }

}
