package com.jsonwizard.interfaces;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by android on 11/26/17.
 */
public interface JsonApi {
    JSONObject getStep(String stepName);

    void writeResult(String stepName, String key, String value) throws JSONException;

    void writeResult(String stepName, String prentKey, String childObjectKey, String childKey, String value)
            throws JSONException;

    String currentJsonState();

    String getCount();
}
