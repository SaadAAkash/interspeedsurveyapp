package com.jsonwizard.interfaces;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.Spinner;

/**
 * Created by android on 11/26/17.
 */
public interface CommonListener extends View.OnClickListener, CompoundButton.OnCheckedChangeListener, Spinner.OnItemSelectedListener {
}
