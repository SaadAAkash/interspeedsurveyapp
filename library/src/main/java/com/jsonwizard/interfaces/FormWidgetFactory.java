package com.jsonwizard.interfaces;

import android.content.Context;
import android.view.View;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by android on 11/26/17.
 */
public interface FormWidgetFactory {
    List<View> getViewsFromJson(String stepName, Context context, JSONObject jsonObject, CommonListener listener) throws Exception;
}
