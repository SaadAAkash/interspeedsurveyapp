package com.jsonwizard.validators.edittext;

import com.rengwuxian.materialedittext.validation.METValidator;

/**
 * Created by android on 11/26/17.
 */
public class LengthValidator extends METValidator {

    int minLength = 0;
    int maxLength = Integer.MAX_VALUE;
    boolean isRequired;

    public LengthValidator(boolean isRequired, String errorMessage, int minLength, int maxLength) {
        super(errorMessage);
        this.isRequired = isRequired;
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    @Override
    public boolean isValid(CharSequence charSequence, boolean isEmpty) {
        if (!isRequired && isEmpty) {
            return true;
        }
        if (!isEmpty) {
            if (charSequence.length() >= minLength && charSequence.length() <= maxLength) {
                return true;
            }
        }
        return false;
    }
}
