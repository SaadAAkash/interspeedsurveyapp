package com.jsonwizard.validators.edittext;

import com.jsonwizard.widgets.EditTextFactory;

/**
 * Created by android on 11/26/17.
 */
public class MaxLengthValidator extends LengthValidator {

    public MaxLengthValidator(boolean isRequired, String errorMessage, int maxLength) {
        super(isRequired, errorMessage, EditTextFactory.MIN_LENGTH, maxLength);
    }
}
