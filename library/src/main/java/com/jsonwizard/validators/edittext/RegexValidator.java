package com.jsonwizard.validators.edittext;

import android.support.annotation.NonNull;

import com.rengwuxian.materialedittext.validation.RegexpValidator;

import java.util.regex.Pattern;

/**
 * Created by android on 11/29/2017.
 */

public class RegexValidator extends RegexpValidator {

    private Pattern pattern;
    private boolean isRequired;

    public RegexValidator(boolean isRequired, @NonNull String errorMessage, @NonNull String regex) {
        super(errorMessage, regex);
        pattern = Pattern.compile(regex);
        this.isRequired = isRequired;
    }

    @Override
    public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
        if (!isRequired && isEmpty) {
            return true;
        }
        return pattern.matcher(text).matches();
    }
}
