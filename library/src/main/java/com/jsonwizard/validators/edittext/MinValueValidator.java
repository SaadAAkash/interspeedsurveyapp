package com.jsonwizard.validators.edittext;

/**
 * Created by android on 11/26/17.
 */

public class MinValueValidator extends ValueValidator {

    public MinValueValidator(boolean isRequired, String errorMessage, int minValue) {
        super(isRequired, errorMessage, minValue, Integer.MAX_VALUE);
    }
}
