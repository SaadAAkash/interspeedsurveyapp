package com.jsonwizard.validators.edittext;

import com.rengwuxian.materialedittext.validation.METValidator;

/**
 * Created by android on 11/26/17.
 */
public class RequiredValidator extends METValidator {

    public RequiredValidator(String errorMessage) {
        super(errorMessage);
    }

    @Override
    public boolean isValid(CharSequence charSequence, boolean isEmpty) {
        return !isEmpty;
    }
}
