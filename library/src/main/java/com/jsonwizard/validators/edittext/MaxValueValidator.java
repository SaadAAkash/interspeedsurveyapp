package com.jsonwizard.validators.edittext;

/**
 * Created by android on 11/26/17.
 */

public class MaxValueValidator extends ValueValidator {

    public MaxValueValidator(boolean isRequired, String errorMessage, int maxLength) {
        super(isRequired, errorMessage, Integer.MIN_VALUE, maxLength);
    }
}
