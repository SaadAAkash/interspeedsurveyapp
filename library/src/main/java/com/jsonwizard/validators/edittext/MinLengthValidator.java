package com.jsonwizard.validators.edittext;

import com.jsonwizard.widgets.EditTextFactory;

/**
 * Created by android on 11/26/17.
 */
public class MinLengthValidator extends LengthValidator {

    public MinLengthValidator(boolean isRequired, String errorMessage, int minLength) {
        super(isRequired, errorMessage, minLength, EditTextFactory.MAX_LENGTH);
    }
}
