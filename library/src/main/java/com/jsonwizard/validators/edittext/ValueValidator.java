package com.jsonwizard.validators.edittext;

import android.support.annotation.NonNull;

import com.rengwuxian.materialedittext.validation.METValidator;

/**
 * Created by android on 11/26/17.
 */

public class ValueValidator extends METValidator {

    int minValue;
    int maxValue;
    boolean isRequired;

    public ValueValidator(boolean isRequired, String errorMessage, int minValue, int maxValue) {
        super(errorMessage);
        this.isRequired = isRequired;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public boolean isValid(@NonNull CharSequence text, boolean isEmpty) {
        if (!isRequired && isEmpty) {
            return true;
        }
        if (!isEmpty) {
            try {
                if (Float.parseFloat(text.toString()) >= minValue
                        && Float.parseFloat(text.toString()) <= maxValue) {
                    return true;
                }
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return false;
    }
}
