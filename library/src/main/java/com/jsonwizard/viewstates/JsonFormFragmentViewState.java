package com.jsonwizard.viewstates;

import android.os.Parcel;

import com.jsonwizard.mvp.ViewState;

/**
 * Created by android on 11/27/17.
 */
public class JsonFormFragmentViewState extends ViewState implements android.os.Parcelable {
    public static final Creator<JsonFormFragmentViewState> CREATOR = new Creator<JsonFormFragmentViewState>() {
        public JsonFormFragmentViewState createFromParcel(
                Parcel source) {
            return new JsonFormFragmentViewState(source);
        }

        public JsonFormFragmentViewState[] newArray(
                int size) {
            return new JsonFormFragmentViewState[size];
        }
    };

    public JsonFormFragmentViewState() {
    }

    private JsonFormFragmentViewState(Parcel in) {
        super(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }
}
