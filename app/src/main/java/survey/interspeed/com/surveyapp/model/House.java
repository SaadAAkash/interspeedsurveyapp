package survey.interspeed.com.surveyapp.model;

import java.util.List;

/**
 * Created by Android on 12/5/2017.
 */

public class House {
    public String name;
    public List<Point> pointList;

    public House(String name, List<Point> pointList) {
        this.name = name;
        this.pointList = pointList;
    }
}
