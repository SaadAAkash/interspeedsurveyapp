package survey.interspeed.com.surveyapp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import survey.interspeed.com.surveyapp.Config;
import survey.interspeed.com.surveyapp.R;
import survey.interspeed.com.surveyapp.database.SqlLite;
import survey.interspeed.com.surveyapp.model.DataModel;
import survey.interspeed.com.surveyapp.service.BestLocation;


/**
 * Created by anupamchugh on 09/02/16.
 */
public class CustomAdapter extends ArrayAdapter<DataModel> implements View.OnClickListener {

    private ArrayList<DataModel> dataSet;
    Context mContext;
    int isRouteUpdated = 0, isLocationUpdated = 0;
    Activity activity;
    String clusterId = "";
    JSONArray jsonArrayLat, jsonArrayLon;
    String latlon[][];
    FormBody.Builder form1;
    OkHttpClient client1;
    SqlLite sqlLite;
    String res[];
    DataModel dataModel;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtType;
        TextView txtVersion;
        ImageView info;
    }


    public CustomAdapter(Activity activity, ArrayList<DataModel> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext = context;
        this.activity = activity;

    }


    @Override
    public void onClick(View v) {


        int position = (Integer) v.getTag();
        Object object = getItem(position);
        dataModel = (DataModel) object;


        switch (v.getId()) {

            case R.id.item_info:

                SharedPreferences status = mContext.getSharedPreferences("status", 0);
                client1 = new OkHttpClient();
                sqlLite = new SqlLite(mContext);
                sqlLite.open();
                res = sqlLite.getRoute(dataModel.getCheckId());
                latlon = sqlLite.getLatLon(dataModel.getCheckId());
                sqlLite.close();
                BestLocation location = new BestLocation(activity);
                OkHttpClient client = new OkHttpClient();
                RequestBody formBody = new FormBody.Builder()
                        .add("regions", res[4])
                        .add("area", res[5])
                        .add("house", res[6])
                        .add("point", res[7])
                        .add("route_name", res[0])
                        .add("route_id", res[1])
                        .add("cluster", res[2])
                        .add("userid", status.getString("cid", ""))
                        .add("teamid", status.getString("sid", ""))
                        .add("lat", String.valueOf(location.getlat()))
                        .add("lon", String.valueOf(location.getLon()))
                        .build();
                final Request request = new Request.Builder()
                        .url(Config.URL + "/clusterinfo.php")
                        .post(formBody)
                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (response.isSuccessful()) {
                            clusterId = response.body().string();
                            int cid = 0;
                            try {
                                cid = Integer.parseInt(clusterId);
                            } catch (Exception e) {

                            }
                            if (cid > 0) {
                                jsonArrayLat = new JSONArray();
                                jsonArrayLon = new JSONArray();
                                for(int f=0;f<latlon[0].length;f++){
                                    jsonArrayLat.put(latlon[0][f]);
                                    jsonArrayLon.put(latlon[1][f]);
                                }
                                form1 = new FormBody.Builder();
                                form1.add("cluster_id", clusterId);
                                form1.add("lat", jsonArrayLat.toString());
                                form1.add("lon", jsonArrayLon.toString());

                                RequestBody formBody1 = form1.build();

                                Request request1 = new Request.Builder()
                                        .url(Config.URL + "/pathinfo.php")
                                        .post(formBody1)
                                        .build();


                                client1.newCall(request1).enqueue(new Callback() {
                                    @Override
                                    public void onFailure(Call call, IOException e) {
                                        showErrorDialog();
                                    }

                                    @Override
                                    public void onResponse(Call call, Response response) throws IOException {
                                        if (response.isSuccessful()) {

                                            if (response.body().string().equals("1")) {
                                                sqlLite.open();
                                                sqlLite.deleteRoute(dataModel.getCheckId());
                                                sqlLite.deleteLocation(dataModel.getCheckId());
                                                sqlLite.close();
                                                activity.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                                        builder.setTitle("Confirmation");
                                                        builder.setCancelable(false);
                                                        builder.setMessage("Data has been uploaded successfully");
                                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                activity.finish();
                                                            }
                                                        });

                                                        builder.show();
                                                    }
                                                });

                                            } else {
                                                showErrorDialog();
                                            }
                                        } else {
                                            showErrorDialog();
                                        }
                                    }
                                });
                            } else {
                                showErrorDialog();

                            }
                        }
                    }
                });

                break;


        }


    }

    public void showErrorDialog(){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("ERROR");
                builder.setCancelable(false);
                builder.setMessage("Data upload has been failed");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                builder.show();
            }
        });
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DataModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {


            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.txtType = (TextView) convertView.findViewById(R.id.type);
            viewHolder.txtVersion = (TextView) convertView.findViewById(R.id.version_number);
            viewHolder.info = (ImageView) convertView.findViewById(R.id.item_info);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;


        viewHolder.txtName.setText(dataModel.getName());
        viewHolder.txtType.setText(dataModel.getType());
        viewHolder.txtVersion.setText(dataModel.getVersion_number());
        viewHolder.info.setOnClickListener(this);
        viewHolder.info.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }


}
