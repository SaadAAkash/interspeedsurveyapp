package survey.interspeed.com.surveyapp.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Android on 12/3/2017.
 */

public class BaseActivity extends AppCompatActivity {

    /**
     * BaseActivity provides the methods for get, save, isContained from Sharedprefs
     * Also, a method for loading JSON from its context and filename
     */
    protected static final String CLIENT_ID = "client_id";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    protected SharedPreferences getPreference() {
        return this.getSharedPreferences("pref2", MODE_PRIVATE);
    }

    protected SharedPreferences.Editor getEditor() {
        return this.getPreference().edit();
    }

    protected boolean saveValue(String key, String value) {
        return getEditor().putString(key, value).commit();
    }

    protected boolean isContained(String key) {
        return getPreference().contains(key);
    }

    protected String getValue(String key) {
        return getPreference().getString(key, null);
    }

    public String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            return null;
        }
        return json;

    }

}
