package survey.interspeed.com.surveyapp.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import survey.interspeed.com.surveyapp.Config;
import survey.interspeed.com.surveyapp.R;
import survey.interspeed.com.surveyapp.boardcastreceiver.AlarmReceiver;
import survey.interspeed.com.surveyapp.database.SqlLite;
import survey.interspeed.com.surveyapp.model.Area;
import survey.interspeed.com.surveyapp.model.House;
import survey.interspeed.com.surveyapp.model.Point;
import survey.interspeed.com.surveyapp.model.Region;
import survey.interspeed.com.surveyapp.model.Route;
import survey.interspeed.com.surveyapp.model.RouteId;
import survey.interspeed.com.surveyapp.service.BestLocation;
import survey.interspeed.com.surveyapp.service.LocationUpdatesService;

/**
 * Loads data from /assets/table.json as lists, adds the form inputs in the list
 */

public class SurveyActivity extends BaseActivity implements SharedPreferences.OnSharedPreferenceChangeListener {


    private static final String TABLE_JSON_PATH = "table.json";

    private MaterialSpinner regionSpinner;
    private MaterialSpinner areaSpinner;
    private MaterialSpinner houseSpinner;
    private MaterialSpinner pointSpinner;
    private MaterialSpinner routeSpinner;
    private MaterialSpinner routeIdSpinner;
    private MaterialSpinner clusterSpinner;

    private Button btnCheckIn;
    private Button btnSelectMap;

    private String regionSelection;
    private String areaSelection;
    private String houseSelection;
    private String pointSelection;
    private String routeSelection;
    private String routeIdSelection;
    private String clusterSelection;

    private Location mLocation;

    private Menu mMenu;
    SqlLite sqlite;
    private static final String TAG = OnboardingActivity.class.getSimpleName();

    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    // A reference to the service used to get location updates.
    private LocationUpdatesService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;
    private static int isRouteUpdated = 0;
    private static int isLocationUpdated = 0;

    String clusterId = "";
    public static Activity activity;

    // Monitors the state of the connection to the service.
    /*private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };*/
    JSONArray jsonArrayLat = new JSONArray();
    JSONArray jsonArrayLon = new JSONArray();
 //   private Realm realm;
    FormBody.Builder form1;
    OkHttpClient client1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
        activity = this;

        regionSpinner = (MaterialSpinner) findViewById(R.id.regionSpinner);
        areaSpinner = (MaterialSpinner) findViewById(R.id.areaSpinner);
        houseSpinner = (MaterialSpinner) findViewById(R.id.houseSpinner);
        pointSpinner = (MaterialSpinner) findViewById(R.id.pointSpinner);
        routeSpinner = (MaterialSpinner) findViewById(R.id.routeSpinner);
        routeIdSpinner = (MaterialSpinner) findViewById(R.id.routeIdSpinner);
        clusterSpinner = (MaterialSpinner) findViewById(R.id.clusterSpinner);

        btnCheckIn = (Button) findViewById(R.id.btnCheckIn);
        btnSelectMap = (Button) findViewById(R.id.btnSelectMap);

        btnCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences status = getSharedPreferences("status", 0);
                sqlite = new SqlLite(SurveyActivity.this);
                sqlite.open();
                String latlon[][] = sqlite.getLatLon();
                sqlite.close();
                BestLocation location = new BestLocation(SurveyActivity.this);
                if (latlon[0] == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SurveyActivity.this);
                    builder.setTitle("ERROR");
                    builder.setCancelable(false);
                    builder.setMessage("Could not find location data for upload");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });

                    builder.show();
                } else {

                    client1 = new OkHttpClient();
                    form1 = new FormBody.Builder();


                    for (int l = 0; l < latlon[0].length; l++) {
                        jsonArrayLat.put(latlon[0][l]);
                        jsonArrayLon.put(latlon[1][l]);
                    }


                    OkHttpClient client = new OkHttpClient();
                    RequestBody formBody = new FormBody.Builder()
                            .add("regions", regionSpinner.getSelectedItem().toString())
                            .add("area", areaSpinner.getSelectedItem().toString())
                            .add("house", houseSpinner.getSelectedItem().toString())
                            .add("point", String.valueOf(pointSpinner.getSelectedItem().toString()))
                            .add("route_name", String.valueOf(routeSpinner.getSelectedItem().toString()))
                            .add("route_id", String.valueOf(routeIdSpinner.getSelectedItem().toString()))
                            .add("cluster", String.valueOf(clusterSpinner.getSelectedItem().toString()))
                            .add("userid", status.getString("cid", ""))
                            .add("teamid", status.getString("sid", ""))
                            .add("lat", String.valueOf(location.getlat()))
                            .add("lon", String.valueOf(location.getLon()))
                            .build();
                    final Request request = new Request.Builder()
                            .url(Config.URL + "/clusterinfo.php")
                            .post(formBody)
                            .build();

                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            SharedPreferences status = getSharedPreferences("status", 0);
                            SharedPreferences.Editor edit = status.edit();
                            edit.putBoolean("isStop", true);
                            edit.commit();
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if (response.isSuccessful()) {

                                clusterId = response.body().string();
                                int cid = 0;
                                try {
                                    cid = Integer.parseInt(clusterId);
                                } catch (Exception e) {

                                }
                                if (cid > 0) {
                                    form1.add("cluster_id", clusterId);
                                    form1.add("lat", jsonArrayLat.toString());
                                    form1.add("lon", jsonArrayLon.toString());
                                    RequestBody formBody1 = form1.build();

                                    Request request1 = new Request.Builder()
                                            .url(Config.URL + "/pathinfo.php")
                                            .post(formBody1)
                                            .build();


                                    client1.newCall(request1).enqueue(new Callback() {
                                        @Override
                                        public void onFailure(Call call, IOException e) {
                                            saveToDb();
                                        }

                                        @Override
                                        public void onResponse(Call call, Response response) throws IOException {
                                            if (response.isSuccessful()) {
                                                SharedPreferences status = getSharedPreferences("status", 0);
                                                SharedPreferences.Editor edit = status.edit();
                                                edit.putBoolean("isStop", true);
                                                edit.commit();
                                                if (response.body().string().equals("1")) {
                                                    sqlite.open();
                                                    sqlite.deleteLocation();
                                                    sqlite.close();
                                                    SurveyActivity.this.runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(SurveyActivity.this);
                                                            builder.setTitle("Confirmation");
                                                            builder.setCancelable(false);
                                                            builder.setMessage("Data has been uploaded successfully");
                                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                                    finish();
                                                                }
                                                            });

                                                            builder.show();
                                                        }
                                                    });

                                                }else {
                                                    saveToDb();
                                                }
                                            } else {
                                                saveToDb();
                                            }
                                        }
                                    });
                                } else {
                                    saveToDb();

                                }


                            }
                        }
                    });
                }

            }
        });

        btnSelectMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences status = getSharedPreferences("status", 0);
                BestLocation location = new BestLocation(SurveyActivity.this);
                Intent intent = new Intent(SurveyActivity.this, MapsActivity2.class);
                intent.putExtra("regions",regionSpinner.getSelectedItem().toString());
                intent.putExtra("area",areaSpinner.getSelectedItem().toString());
                intent.putExtra("house",houseSpinner.getSelectedItem().toString());
                intent.putExtra("point",String.valueOf(pointSpinner.getSelectedItem().toString()));
                intent.putExtra("route_name",String.valueOf(routeSpinner.getSelectedItem().toString()));
                intent.putExtra("route_id",String.valueOf(routeIdSpinner.getSelectedItem().toString()));
                intent.putExtra("cluster",String.valueOf(clusterSpinner.getSelectedItem().toString()));
                intent.putExtra("userid",status.getString("cid", ""));
                intent.putExtra("teamid",status.getString("sid", ""));
                intent.putExtra("lat",String.valueOf(location.getlat()));
                intent.putExtra("lon",String.valueOf(location.getLon()));


                if (gpsEnabled()) {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        // Do something for lollipop and above versions
                        // Check that the user hasn't revoked permissions by going to Settings.

                        if (!checkPermissions()) {
                            requestPermissions();
                        } else {
                            startActivity(intent);
                        }
                    } else {
                        startActivity(intent);
                    }
                }
            }
        });

        List<Region> regionList = getTableFromJson(loadJSONFromAsset(getApplicationContext(), TABLE_JSON_PATH));

        setRegionSpinner(regionList);

     //   realm = Realm.getDefaultInstance();
    }

    private boolean gpsEnabled() {
        if (!((LocationManager) getSystemService(LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(SurveyActivity.this, "Turn on GPS", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            return false;
        }
        return true;
    }

    private boolean checkPermissions() {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(R.id.activity_onboarding),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(SurveyActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(SurveyActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
       /* bindService(new Intent(this, LocationUpdatesService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);*/
    }

    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            //     unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
    }

    private boolean isValidate() {
        if (TextUtils.isEmpty(regionSelection) || TextUtils.isEmpty(areaSelection)
                || TextUtils.isEmpty(houseSelection) || TextUtils.isEmpty(pointSelection)
                || TextUtils.isEmpty(routeSelection) || TextUtils.isEmpty(routeIdSelection)
                || TextUtils.isEmpty(clusterSelection)) {
            return false;
        }
        return true;
    }

    private List<Region> getTableFromJson(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray regionArray = jsonObject.getJSONArray("region");
            List<Region> regionList = new ArrayList<>();
            for (int i = 0; i < regionArray.length(); i++) {
                JSONArray areaArray = regionArray.getJSONObject(i).getJSONArray("area");
                List<Area> areaList = new ArrayList<>();
                for (int j = 0; j < areaArray.length(); j++) {
                    JSONArray houseArray = areaArray.getJSONObject(j).getJSONArray("house");
                    List<House> houseList = new ArrayList<>();
                    for (int k = 0; k < houseArray.length(); k++) {
                        JSONArray pointArray = houseArray.getJSONObject(k).getJSONArray("point");
                        List<Point> pointList = new ArrayList<>();
                        for (int l = 0; l < pointArray.length(); l++) {
                            JSONArray routeArray = pointArray.getJSONObject(l).getJSONArray("route");
                            List<Route> routeList = new ArrayList<>();
                            for (int m = 0; m < routeArray.length(); m++) {
                                JSONArray routeIdArray = routeArray.getJSONObject(m).getJSONArray("route_id");
                                List<RouteId> routeIdList = new ArrayList<>();
                                for (int n = 0; n < routeIdArray.length(); n++) {
                                    JSONArray clusterArray = routeIdArray.getJSONObject(n).getJSONArray("cluster");
                                    List<String> clusterList = new ArrayList<>();
                                    for (int o = 0; o < clusterArray.length(); o++) {
                                        clusterList.add(clusterArray.getString(o));
                                    }
                                    routeIdList.add(new RouteId(routeIdArray.getJSONObject(n).getString("name"), clusterList));
                                }
                                routeList.add(new Route(routeArray.getJSONObject(m).getString("name"), routeIdList));
                            }
                            pointList.add(new Point(pointArray.getJSONObject(l).getString("name"), routeList));
                        }
                        houseList.add(new House(houseArray.getJSONObject(k).getString("name"), pointList));
                    }
                    areaList.add(new Area(areaArray.getJSONObject(j).getString("name"), houseList));
                }
                regionList.add(new Region(regionArray.getJSONObject(i).getString("name"), areaList));
            }
            return regionList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void setRegionSpinner(final List<Region> regionList) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getRegionList(regionList));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        regionSpinner.setAdapter(adapter);
        regionSpinner.setSelection(adapter.getCount() > 0 ? 1 : 0);

        regionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    setAreaSpinner(regionList.get(position));
                    regionSelection = regionList.get(position).name;
                } catch (ArrayIndexOutOfBoundsException e) {
                    // skip
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String[] getRegionList(List<Region> regionList) {
        String[] region = new String[regionList.size()];
        for (int i = 0; i < regionList.size(); i++) {
            region[i] = regionList.get(i).name;
        }
        return region;
    }

    private void setAreaSpinner(final Region region) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getAreaList(region));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        areaSpinner.setAdapter(adapter);
        areaSpinner.setSelection(adapter.getCount() > 0 ? 1 : 0);

        areaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    setHouseSpinner(region.areaList.get(position));
                    areaSelection = region.areaList.get(position).name;
                } catch (ArrayIndexOutOfBoundsException e) {
                    // skip
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String[] getAreaList(Region region) {
        String[] area = new String[region.areaList.size()];
        for (int i = 0; i < region.areaList.size(); i++) {
            area[i] = region.areaList.get(i).name;
        }
        return area;
    }

    private void setHouseSpinner(final Area area) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getHouseList(area));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        houseSpinner.setAdapter(adapter);
        houseSpinner.setSelection(adapter.getCount() > 0 ? 1 : 0);

        houseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    setPointSpinner(area.houseList.get(position));
                    houseSelection = area.houseList.get(position).name;
                } catch (ArrayIndexOutOfBoundsException e) {
                    // skip
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private String[] getHouseList(Area area) {
        String[] house = new String[area.houseList.size()];
        for (int i = 0; i < area.houseList.size(); i++) {
            house[i] = area.houseList.get(i).name;
        }
        return house;
    }

    private void setPointSpinner(final House house) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getPointList(house));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pointSpinner.setAdapter(adapter);
        pointSpinner.setSelection(adapter.getCount() > 0 ? 1 : 0);

        pointSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    setRouteSpinner(house.pointList.get(position));
                    pointSelection = house.pointList.get(position).name;
                } catch (ArrayIndexOutOfBoundsException e) {
                    // skip
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String[] getPointList(House house) {
        String[] point = new String[house.pointList.size()];
        for (int i = 0; i < house.pointList.size(); i++) {
            point[i] = house.pointList.get(i).name;
        }
        return point;
    }

    private void setRouteSpinner(final Point point) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getRouteList(point));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        routeSpinner.setAdapter(adapter);
        routeSpinner.setSelection(adapter.getCount() > 0 ? 1 : 0);

        routeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    setRouteIdSpinner(point.routeList.get(position));
                    routeSelection = point.routeList.get(position).name;
                } catch (ArrayIndexOutOfBoundsException e) {
                    // skip
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String[] getRouteList(Point point) {
        String[] route = new String[point.routeList.size()];
        for (int i = 0; i < point.routeList.size(); i++) {
            route[i] = point.routeList.get(i).name;
        }
        return route;
    }

    private void setRouteIdSpinner(final Route route) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getRouteIdList(route));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        routeIdSpinner.setAdapter(adapter);
        routeIdSpinner.setSelection(adapter.getCount() > 0 ? 1 : 0);

        routeIdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    setClusterSpinner(route.routeIdList.get(position));
                    routeIdSelection = route.routeIdList.get(position).name;
                } catch (ArrayIndexOutOfBoundsException e) {
                    // skip
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String[] getRouteIdList(Route route) {
        String[] routeId = new String[route.routeIdList.size()];
        for (int i = 0; i < route.routeIdList.size(); i++) {
            routeId[i] = route.routeIdList.get(i).name;
        }
        return routeId;
    }


    private void setClusterSpinner(final RouteId routeId) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getClusterList(routeId));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        clusterSpinner.setAdapter(adapter);
        clusterSpinner.setSelection(adapter.getCount() > 0 ? 1 : 0);

        clusterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    clusterSelection = routeId.clusterList.get(position);
                } catch (ArrayIndexOutOfBoundsException e) {
                    // skip
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String[] getClusterList(RouteId routeId) {
        String[] cluster = new String[routeId.clusterList.size()];
        for (int i = 0; i < routeId.clusterList.size(); i++) {
            cluster[i] = routeId.clusterList.get(i);
        }
        return cluster;
    }

    private void saveToDb() {

        SqlLite db = new SqlLite(SurveyActivity.this);
        db.open();
        db.createEntryRoute(regionSpinner.getSelectedItem().toString(), areaSpinner.getSelectedItem().toString(), houseSpinner.getSelectedItem().toString(), pointSpinner.getSelectedItem().toString(), routeSpinner.getSelectedItem().toString(), routeIdSpinner.getSelectedItem().toString(), clusterSpinner.getSelectedItem().toString());
        db.close();
        SharedPreferences status = getSharedPreferences("status", 0);
        SharedPreferences.Editor edit = status.edit();
        edit.putBoolean("isStop", true);
        edit.commit();

        SurveyActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(SurveyActivity.this);
                builder.setTitle("Confirmation");
                builder.setCancelable(false);
                builder.setMessage("Upload failed. Data has been saved locally");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });

                builder.show();
            }
        });

        Intent alarm = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarm, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        /*realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                CheckIn checkIn = realm.createObject(CheckIn.class, UUID.randomUUID().toString());
                checkIn.setClientId(getValue(CLIENT_ID));
                checkIn.setDate(new Date());
                checkIn.setLat(mLocation.getLatitude());
                checkIn.setLng(mLocation.getLongitude());
                checkIn.setRegion(regionSpinner.getSelectedItem().toString());
                checkIn.setArea(areaSpinner.getSelectedItem().toString());
                checkIn.setHouse(houseSpinner.getSelectedItem().toString());
                checkIn.setPoint(pointSpinner.getSelectedItem().toString());
                checkIn.setRoute(routeSpinner.getSelectedItem().toString());
                checkIn.setRouteId(routeIdSpinner.getSelectedItem().toString());
                checkIn.setCluster(clusterSpinner.getSelectedItem().toString());
            }
        });*/


    }
}
