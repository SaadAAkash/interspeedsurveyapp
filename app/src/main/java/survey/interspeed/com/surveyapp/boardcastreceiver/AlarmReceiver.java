package survey.interspeed.com.surveyapp.boardcastreceiver;

/**
 * Created by Biplob on 12/23/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import survey.interspeed.com.surveyapp.WakeLocker.WakeLocker;
import survey.interspeed.com.surveyapp.database.SqlLite;
import survey.interspeed.com.surveyapp.service.BestLocation;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
      //  Intent background = new Intent(context, MyLocationServices.class);
       // context.startService(background);
        WakeLocker.acquire(context);BestLocation location=new BestLocation(context);
        SqlLite db = new SqlLite(context);
        db.open();
        if(location.getLon()!=0.0 && location.getlat()!=0.0)
            if(!db.checkexistinglatlon(String.valueOf(location.getlat()),String.valueOf(location.getLon())))
               db.createEntryLocation(String.valueOf(location.getlat()),String.valueOf(location.getLon()));
        db.close();
     //   Toast.makeText(context,String.valueOf(location.getlat())+","+String.valueOf(location.getLon()),Toast.LENGTH_LONG).show();
    }

}