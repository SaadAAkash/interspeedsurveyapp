package survey.interspeed.com.surveyapp.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

////////////////////
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import android.location.Location;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;
////////////////////

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import survey.interspeed.com.surveyapp.Config;
import survey.interspeed.com.surveyapp.MapParser.DirectionsJSONParser;
import survey.interspeed.com.surveyapp.R;
import survey.interspeed.com.surveyapp.boardcastreceiver.AlarmReceiver;
import survey.interspeed.com.surveyapp.database.SqlLite;
import survey.interspeed.com.surveyapp.service.BestLocation;

public class MapsActivity2 extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    ////////////////
    private Location loc;
    private Circle mCircle;
    private Context mContext;
    ////////////////
    Button upload,cancel ;
    Bundle extra;
    SqlLite sqlite;
    JSONArray jsonArrayLat = new JSONArray();
    JSONArray jsonArrayLon = new JSONArray();
    //   private Realm realm;
    FormBody.Builder form1;
    OkHttpClient client1;
    String clusterId = "";
    String regions,area,house,point,route_name,route_id,cluster,userid,teamid,lat,lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps2);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        upload = (Button)findViewById(R.id.btnUpload);
        cancel = (Button)findViewById(R.id.btnCancel);

        extra = getIntent().getExtras();
        if(extra != null){
            regions = extra.getString("regions");
            area = extra.getString("area");
            house = extra.getString("house");
            point = extra.getString("point");
            route_name = extra.getString("route_name");
            route_id = extra.getString("route_id");
            cluster = extra.getString("cluster");
            userid = extra.getString("userid");
            teamid = extra.getString("teamid");
            lat = extra.getString("lat");
            lon = extra.getString("lon");
        }

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences status = getSharedPreferences("status", 0);
                sqlite = new SqlLite(MapsActivity2.this);
                sqlite.open();
                String latlon[][] = sqlite.getLatLon();
                sqlite.close();
                BestLocation location = new BestLocation(MapsActivity2.this);
                if (latlon[0] == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity2.this);
                    builder.setTitle("ERROR");
                    builder.setCancelable(false);
                    builder.setMessage("Could not find location data for upload");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });

                    builder.show();
                } else {

                    client1 = new OkHttpClient();
                    form1 = new FormBody.Builder();


                    for (int l = 0; l < latlon[0].length; l++) {
                        jsonArrayLat.put(latlon[0][l]);
                        jsonArrayLon.put(latlon[1][l]);
                    }


                    OkHttpClient client = new OkHttpClient();
                    RequestBody formBody = new FormBody.Builder()
                            .add("regions", regions)
                            .add("area", area)
                            .add("house", house)
                            .add("point", point)
                            .add("route_name", route_name)
                            .add("route_id", route_id)
                            .add("cluster", cluster)
                            .add("userid", userid)
                            .add("teamid", teamid)
                            .add("lat", lat)
                            .add("lon", lon)
                            .build();
                    final Request request = new Request.Builder()
                            .url(Config.URL + "/clusterinfo.php")
                            .post(formBody)
                            .build();


                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            SharedPreferences status = getSharedPreferences("status", 0);
                            SharedPreferences.Editor edit = status.edit();
                            edit.putBoolean("isStop", true);
                            edit.commit();
                            Toast.makeText(getApplicationContext(), "ON FAILURE", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {

                            Toast.makeText(getApplicationContext(), "ON RESPONSE", Toast.LENGTH_SHORT).show();
                            if (response.isSuccessful()) {

                                clusterId = response.body().string();
                                int cid = 0;
                                try {
                                    cid = Integer.parseInt(clusterId);
                                } catch (Exception e) {

                                }
                                if (cid > 0) {
                                    form1.add("cluster_id", clusterId);
                                    form1.add("lat", jsonArrayLat.toString());
                                    form1.add("lon", jsonArrayLon.toString());
                                    RequestBody formBody1 = form1.build();

                                    Request request1 = new Request.Builder()
                                            .url(Config.URL + "/pathinfo.php")
                                            .post(formBody1)
                                            .build();


                                    client1.newCall(request1).enqueue(new Callback() {
                                        @Override
                                        public void onFailure(Call call, IOException e) {
                                            saveToDb(regions,area,house,point,route_name,route_id,cluster);
                                        }

                                        @Override
                                        public void onResponse(Call call, Response response) throws IOException {
                                            if (response.isSuccessful()) {
                                                SharedPreferences status = getSharedPreferences("status", 0);
                                                SharedPreferences.Editor edit = status.edit();
                                                edit.putBoolean("isStop", true);
                                                edit.commit();
                                                if (response.body().string().equals("1")) {
                                                    sqlite.open();
                                                    sqlite.deleteLocation();
                                                    sqlite.close();
                                                    MapsActivity2.this.runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity2.this);
                                                            builder.setTitle("Confirmation");
                                                            builder.setCancelable(false);
                                                            builder.setMessage("Data has been uploaded successfully");
                                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                                    finish();
                                                                    SurveyActivity.activity.finish();
                                                                }
                                                            });

                                                            builder.show();
                                                        }
                                                    });

                                                }else {
                                                    saveToDb(regions,area,house,point,route_name,route_id,cluster);
                                                }
                                            } else {
                                                saveToDb(regions,area,house,point,route_name,route_id,cluster);
                                            }
                                        }
                                    });
                                } else {
                                    saveToDb(regions,area,house,point,route_name,route_id,cluster);

                                }


                            }
                        }
                    });
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    private void saveToDb(String regions,String area,String house,String point,String route_name,String route_id,String cluster) {

        SqlLite db = new SqlLite(MapsActivity2.this);
        db.open();
        db.createEntryRoute(regions, area, house, point, route_name, route_id, cluster);
        db.close();
        SharedPreferences status = getSharedPreferences("status", 0);
        SharedPreferences.Editor edit = status.edit();
        edit.putBoolean("isStop", true);
        edit.commit();

        MapsActivity2.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity2.this);
                builder.setTitle("Confirmation");
                builder.setCancelable(false);
                builder.setMessage("Upload failed. Data has been saved locally");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        SurveyActivity.activity.finish();

                    }
                });

                builder.show();
            }
        });

        Intent alarm = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarm, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        /*realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                CheckIn checkIn = realm.createObject(CheckIn.class, UUID.randomUUID().toString());
                checkIn.setClientId(getValue(CLIENT_ID));
                checkIn.setDate(new Date());
                checkIn.setLat(mLocation.getLatitude());
                checkIn.setLng(mLocation.getLongitude());
                checkIn.setRegion(regionSpinner.getSelectedItem().toString());
                checkIn.setArea(areaSpinner.getSelectedItem().toString());
                checkIn.setHouse(houseSpinner.getSelectedItem().toString());
                checkIn.setPoint(pointSpinner.getSelectedItem().toString());
                checkIn.setRoute(routeSpinner.getSelectedItem().toString());
                checkIn.setRouteId(routeIdSpinner.getSelectedItem().toString());
                checkIn.setCluster(clusterSpinner.getSelectedItem().toString());
            }
        });*/


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        BestLocation location=new BestLocation(this);
        LatLng latLng = new LatLng(location.getlat(), location.getLon());

        ////////////////
        drawMarkerWithCircle(latLng);
        ////////////////

        //mMap.addMarker(new MarkerOptions().position(latLng).title("My Position"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
        drawroadmap();
    }


    ////////////////
    ////////////////
    private void drawMarkerWithCircle(LatLng position) {

        loc = new Location("dummyprovider");
        float accuracy = loc.getAccuracy() ; //returns accuracy in meters, there is 68% chance of which is correct
        double radiusInMeters = (double)(accuracy+10);  // increase decrease this distancce as per your requirements
        String str = "Accuracy: " + (accuracy+10) + "m";

        int strokeColor = 0xffff0000; //red outline
        int shadeColor = 0x44ff0000; //opaque red fill
        Toast.makeText(getApplicationContext(), str , Toast.LENGTH_LONG).show();

        CircleOptions circleOptions = new CircleOptions()
                .center(position)
                .radius(radiusInMeters)
                .fillColor(shadeColor)
                .strokeColor(strokeColor)
                .strokeWidth(8);
        mCircle = mMap.addCircle(circleOptions);
        ////////////////
        MarkerOptions markerOptions = new MarkerOptions().position(position).title("My Position");
        mMap.addMarker(markerOptions);
        ////////////////
    }
    ////////////////
    ////////////////

    private void drawroadmap(){
        ArrayList<LatLng> points = null;


       /* for(int i=0;i<result.size();i++){*/
            points = new ArrayList<LatLng>();
        SqlLite sqlLite = new SqlLite(MapsActivity2.this);
        sqlLite.open();
        String result[][] = sqlLite.getLatLon();
        sqlLite.close();

        if(result != null){
            if(result[0]!=null)
            for(int k =0 ; k<result[0].length;k++){
                points.add(new LatLng(Double.parseDouble(result[0][k]),Double.parseDouble(result[1][k])));
            }
        }

        if(points.size()>=2) {
            String url = getDirectionsUrl(points.get(0), points.get(points.size()-1));

            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);
        }
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){


        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }

    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            //Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
}
