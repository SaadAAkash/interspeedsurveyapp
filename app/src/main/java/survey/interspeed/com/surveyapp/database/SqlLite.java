package survey.interspeed.com.surveyapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class SqlLite {

    public static final String KEY_ROWID = "_id";
    public static final String KEY_USERID = "";
    //public static final String KEY_HOTNESS="milli_sec";
    public static final String KEY_CHECKINID = "C_ID";
    public static final String KEY_REGION = "REGIONS";
    public static final String KEY_AREA = "AREA";
    public static final String KEY_HOUSE = "HOUSE";
    public static final String KEY_POINT = "POINT";
    public static final String KEY_ROUTE = "ROUTE_NAME";
    public static final String KEY_ROUTEID = "ROUTE_ID";
    public static final String KEY_CLUSTER = "CLUSTER";
    public static final String KEY_LAT = "LATITUDE";
    public static final String KEY_LON = "LONGITURE";
    public static final String KEY_PATHID = "P_ID";
    public static final String KEY_STATUS = "STATUS";
    public static final String KEY_EXTRA2 = "status";
    public static final String KEY_EXTRA3 = "day_count";
    public static final String KEY_EXTRA4 = "alarm_name_set";
    public static final String KEY_EXTRA5 = "alarm_order";


    private static final String DATABASE_NAME = "SURVEY";
    private static final String DATABASE_TABLE_ROUTE = "ROUTE_MST";
    private static final String DATABASE_TABLE_LOCATION = "ROUTE_DTL";
    private static final int DATABASE_VERSION = 1;
    public static Context c;
    private DbHelper dbhelper;
    private Context ourcontext;
    private SQLiteDatabase ourdatabase;

    private static boolean disableNotification = false;


    private static class DbHelper extends SQLiteOpenHelper {


        public DbHelper(Context context) {

            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            // TODO Auto-generated constructor stub
            c = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub
            db.execSQL("CREATE TABLE " + DATABASE_TABLE_ROUTE + " (" +
                    KEY_CHECKINID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_REGION + " TEXT, " +
                    KEY_AREA + " TEXT, " +
                    KEY_HOUSE + " TEXT, " +
                    KEY_POINT + " TEXT, " +
                    KEY_ROUTE + " TEXT, " +
                    KEY_ROUTEID + " TEXT,  " +
                    KEY_CLUSTER + " TEXT );"
            );

            db.execSQL("CREATE TABLE " + DATABASE_TABLE_LOCATION + " (" +
                    KEY_PATHID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_CHECKINID + " TEXT, " +
                    KEY_LAT + " TEXT,  " +
                    KEY_LON + " TEXT,  " +
                    KEY_STATUS + " TEXT );"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
            db.execSQL("DROP TABLE IF EXISTS '" + DATABASE_TABLE_ROUTE + "'");
            onCreate(db);
        }

    }

    public SqlLite(Context c) {

        ourcontext = c;

    }

    public SqlLite open() {

        dbhelper = new DbHelper(ourcontext);
        ourdatabase = dbhelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbhelper.close();
    }

    public long createEntryRoute(String region,String area,String house,String point,String ROUTE_NAME, String ROUTE_ID, String CLUSTER) {
        // TODO Auto-generated method stub
        ContentValues cv = new ContentValues();
        cv.put(KEY_REGION, region);
        cv.put(KEY_AREA, area);
        cv.put(KEY_HOUSE, house);
        cv.put(KEY_POINT, point);
        cv.put(KEY_ROUTE, ROUTE_NAME);
        cv.put(KEY_ROUTEID, ROUTE_ID);
        cv.put(KEY_CLUSTER, CLUSTER);

        long idd = ourdatabase.insert(DATABASE_TABLE_ROUTE, null, cv);
        if(idd!=-1){
            updateEntryLocation(getCheckID());
        }
        return idd;
    }

    public long createEntryLocation(String lat, String lon) {
        // TODO Auto-generated method stub
        ContentValues cv = new ContentValues();
        cv.put(KEY_LAT, lat);
        cv.put(KEY_LON, lon);
        cv.put(KEY_STATUS, "I");


        long idd = ourdatabase.insert(DATABASE_TABLE_LOCATION, null, cv);
        return idd;
    }

    public void updateEntryLocation(String c_id) {
        ContentValues cv = new ContentValues();
        if (c_id != null) {
            cv.put(KEY_CHECKINID, c_id);
        }
        cv.put(KEY_STATUS, "C");

        ourdatabase.update(DATABASE_TABLE_LOCATION, cv, KEY_STATUS + " = " +"'I'", null);

    }

    public String getCheckID() {
        // TODO Auto-generated method stub
        //String[] columns = new String[]{KEY_ROWID , KEY_NAME , KEY_HOTNESS };alarm_order
        //Cursor read = ourdatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);
        String getdata = "Select max("+KEY_CHECKINID+") as c_id from " + DATABASE_TABLE_ROUTE ;
        Cursor read = ourdatabase.rawQuery(getdata, null);
        String result = "";

        int c_id = read.getColumnIndex("c_id");
        for (read.moveToFirst(); !read.isAfterLast(); read.moveToNext()) {
            result=read.getString(c_id);
        }

        return result;
    }

    public boolean checkexistinglatlon(String lating,String loning) {
        // TODO Auto-generated method stub
        //String[] columns = new String[]{KEY_ROWID , KEY_NAME , KEY_HOTNESS };alarm_order
        //Cursor read = ourdatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);
        String getdata = "Select * from " + DATABASE_TABLE_LOCATION + " where "+KEY_LAT+"='"+lating +"' and "+KEY_LON+"='"+loning+"' and "+ KEY_STATUS+"='I'";
        Cursor read = ourdatabase.rawQuery(getdata, null);

        boolean result = false;
        //Toast.makeText(ourcontext, String.valueOf(p), Toast.LENGTH_SHORT).show();

        int i = 0;
        int lat = read.getColumnIndex(KEY_LAT);
        int lon = read.getColumnIndex(KEY_LON);

        String time = "";
        for (read.moveToFirst(); !read.isAfterLast(); read.moveToNext()) {
            result = true;
            i++;
        }


        return result;
    }

    public String[][] getLatLon() {
        // TODO Auto-generated method stub
        //String[] columns = new String[]{KEY_ROWID , KEY_NAME , KEY_HOTNESS };alarm_order
        //Cursor read = ourdatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);
        String getdata = "Select "+KEY_LON+" , "+KEY_LAT+","+KEY_PATHID+" from " + DATABASE_TABLE_LOCATION +" where "+KEY_STATUS+"= 'I' order by "+KEY_PATHID+" ASC";
        Cursor read = ourdatabase.rawQuery(getdata, null);


        int a = read.getCount();
        int flag = 0;

        String result[][] = new String[2][a];
        //Toast.makeText(ourcontext, String.valueOf(p), Toast.LENGTH_SHORT).show();

        int i = 0;
        int lat = read.getColumnIndex(KEY_LAT);
        int lon = read.getColumnIndex(KEY_LON);
        int pathid = read.getColumnIndex(KEY_PATHID);


        String time = "";
        for (read.moveToFirst(); !read.isAfterLast(); read.moveToNext()) {

                result[0][i] = read.getString(lat);
                result[1][i] = read.getString(lon);

                 i++;
            }


        return result;
    }


    public String[][] getLatLon(String chid) {

        String getdata = "Select "+KEY_LON+" , "+KEY_LAT+","+KEY_PATHID+" from " + DATABASE_TABLE_LOCATION +" where "+KEY_CHECKINID+" = '"+chid+"' order by "+KEY_PATHID+" ASC";
        Cursor read = ourdatabase.rawQuery(getdata, null);


        int a = read.getCount();
        String result[][] = new String[2][a];

        int i = 0;
        int lat = read.getColumnIndex(KEY_LAT);
        int lon = read.getColumnIndex(KEY_LON);

        for (read.moveToFirst(); !read.isAfterLast(); read.moveToNext()) {

            result[0][i] = read.getString(lat);
            result[1][i] = read.getString(lon);

            i++;
        }


        return result;
    }


    public String[][] getRoute() {
        // TODO Auto-generated method stub
        //String[] columns = new String[]{KEY_ROWID , KEY_NAME , KEY_HOTNESS };alarm_order
        //Cursor read = ourdatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);
        String getdata = "Select * from " + DATABASE_TABLE_ROUTE ;
        Cursor read = ourdatabase.rawQuery(getdata, null);


        int a = read.getCount();
        int flag = 0;

        String result[][] = new String[4][a];
        //Toast.makeText(ourcontext, String.valueOf(p), Toast.LENGTH_SHORT).show();

        int i = 0;
        int rou = read.getColumnIndex(KEY_ROUTE);
        int rouid = read.getColumnIndex(KEY_ROUTEID);
        int clus = read.getColumnIndex(KEY_CLUSTER);
        int id = read.getColumnIndex(KEY_CHECKINID);

        String time = "";
        for (read.moveToFirst(); !read.isAfterLast(); read.moveToNext()) {
            result[0][i] = read.getString(rou);
            result[1][i] = read.getString(rouid);
            result[2][i] = read.getString(clus);
            result[3][i] = read.getString(id);
            i++;
        }


        return result;
    }


    public String[] getRoute(String chid) {
        // TODO Auto-generated method stub
        //String[] columns = new String[]{KEY_ROWID , KEY_NAME , KEY_HOTNESS };alarm_order
        //Cursor read = ourdatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);
        String getdata = "Select * from " + DATABASE_TABLE_ROUTE +" where "+KEY_CHECKINID+"="+chid;
        Cursor read = ourdatabase.rawQuery(getdata, null);

        String result[] = new String[8];
        int rou = read.getColumnIndex(KEY_ROUTE);
        int rouid = read.getColumnIndex(KEY_ROUTEID);
        int clus = read.getColumnIndex(KEY_CLUSTER);
        int id = read.getColumnIndex(KEY_CHECKINID);
        int reg = read.getColumnIndex(KEY_REGION);
        int area = read.getColumnIndex(KEY_AREA);
        int house = read.getColumnIndex(KEY_HOUSE);
        int point = read.getColumnIndex(KEY_POINT);

        String time = "";
        for (read.moveToFirst(); !read.isAfterLast(); read.moveToNext()) {
            result[0] = read.getString(rou);
            result[1] = read.getString(rouid);
            result[2] = read.getString(clus);
            result[3] = read.getString(id);
            result[4] = read.getString(reg);
            result[5] = read.getString(area);
            result[6] = read.getString(house);
            result[7] = read.getString(point);
        }


        return result;
    }

    public void deleteLocation() {
        String getdata = "delete from " + DATABASE_TABLE_LOCATION + " where "+KEY_CHECKINID+ " is null" ;
        ourdatabase.execSQL(getdata);
    }

    public void deleteLocation(String chid) {
        String getdata = "delete from " + DATABASE_TABLE_LOCATION + " where "+KEY_CHECKINID+ " = '"+chid+"'" ;
        ourdatabase.execSQL(getdata);
    }

    public void deleteRoute(String chid) {
        String getdata = "delete from " + DATABASE_TABLE_ROUTE + " where "+KEY_CHECKINID+ " = '"+chid+"'" ;
        ourdatabase.execSQL(getdata);
    }

}

