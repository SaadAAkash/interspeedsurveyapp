package survey.interspeed.com.surveyapp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import survey.interspeed.com.surveyapp.Config;
import survey.interspeed.com.surveyapp.R;
import survey.interspeed.com.surveyapp.database.SqlLite;
import survey.interspeed.com.surveyapp.model.DataModel;
import survey.interspeed.com.surveyapp.service.BestLocation;

/**
 *
 */
public class CheckInList extends AppCompatActivity {
    ArrayList<DataModel> dataModels;
    ListView listView;
    Button uploadall;
    private static CustomAdapter adapter;

    String clusterId = "";
    JSONArray jsonArrayLat;
    JSONArray jsonArrayLon;
    //   private Realm realm;
    FormBody.Builder form1;

    String res[], latlon[][], routemst[][];
    int k = 0;
    SqlLite sqlite;
    Response response,response1 ;
    BestLocation location ;
    OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_list);

        listView = (ListView) findViewById(R.id.list);
        uploadall = (Button) findViewById(R.id.button2);
        location = new BestLocation(CheckInList.this);

        dataModels = new ArrayList<>();
        SqlLite db = new SqlLite(CheckInList.this);
        db.open();
        String allRoutes[][] = db.getRoute();
        db.close();

        for (int i = 0; i < allRoutes[0].length; i++) {
            dataModels.add(new DataModel(allRoutes[0][i], allRoutes[1][i], allRoutes[2][i], allRoutes[3][i]));
        }

        adapter = new CustomAdapter(this, dataModels, this);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DataModel dataModel = dataModels.get(position);

                Snackbar.make(view, dataModel.getName() + "\n" + dataModel.getType() + "  " + dataModel.getVersion_number(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
            }
        });

        uploadall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                UploadAllData uploadAllData = new UploadAllData(CheckInList.this);
                uploadAllData.execute();

            }
        });
    }

    public void showErrorDialog(){

        CheckInList.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(CheckInList.this);
                builder.setTitle("ERROR");
                builder.setCancelable(false);
                builder.setMessage("Data upload has been failed");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                builder.show();
            }
        });
    }

    public class UploadAllData extends AsyncTask<Void,String,Integer>{

        int isError = 2;
        Context mContext;
        ProgressDialog progress;

        public UploadAllData(Context mContext){
            this.mContext = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(mContext);
            progress.setMessage("Uploading data.. ");
            progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progress.setIndeterminate(true);

        }

        @Override
        protected Integer doInBackground(Void... voids) {

            SharedPreferences status = getSharedPreferences("status", 0);

            SqlLite sqlLite = new SqlLite(CheckInList.this);
            sqlLite.open();
            routemst = sqlLite.getRoute();
            sqlite = new SqlLite(CheckInList.this);
            for (k = 0; k < routemst[3].length; k++) {
                res = sqlLite.getRoute(routemst[3][k]);
                latlon = sqlLite.getLatLon(routemst[3][k]);


                if (latlon[0] == null) {
                    isError = 1;
                    break;
                } else {


                    RequestBody formBody = new FormBody.Builder()
                            .add("regions", res[4])
                            .add("area", res[5])
                            .add("house", res[6])
                            .add("point", res[7])
                            .add("route_name", res[0])
                            .add("route_id", res[1])
                            .add("cluster", res[2])
                            .add("userid", status.getString("cid", ""))
                            .add("teamid", status.getString("sid", ""))
                            .add("lat", String.valueOf(location.getlat()))
                            .add("lon", String.valueOf(location.getLon()))
                            .build();
                    final Request request = new Request.Builder()
                            .url(Config.URL + "/clusterinfo.php")
                            .post(formBody)
                            .build();

                    try {
                        response = client.newCall(request).execute();
                        clusterId = response.body().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (response != null && response.isSuccessful()) {

                        int cid = 0;
                        try {
                            cid = Integer.parseInt(clusterId);
                        } catch (Exception e) {

                        }
                        if (cid > 0) {
                            jsonArrayLat = new JSONArray();
                            jsonArrayLon = new JSONArray();
                            for(int f=0;f<latlon[0].length;f++){
                                jsonArrayLat.put(latlon[0][f]);
                                jsonArrayLon.put(latlon[1][f]);
                            }
                            form1 = new FormBody.Builder();
                            form1.add("cluster_id", clusterId);
                            form1.add("lat", jsonArrayLat.toString());
                            form1.add("lon", jsonArrayLon.toString());

                            RequestBody formBody1 = form1.build();

                            Request request1 = new Request.Builder()
                                    .url(Config.URL + "/pathinfo.php")
                                    .post(formBody1)
                                    .build();
                            String result = "";
                            try {
                                response1 = client.newCall(request1).execute();
                                result = response1.body().string();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            if (response1 != null && response1.isSuccessful()) {

                                if (result.equals("1")) {
                                    isError=0;
                                    sqlite.open();
                                    sqlite.deleteRoute(routemst[3][k]);
                                    sqlite.deleteLocation(routemst[3][k]);
                                    sqlite.close();

                                } else {

                                    isError=1;
                                    break;
                                }
                            } else {

                                isError=1;
                                break;
                            }


                        } else {
                            isError=1;
                            break;

                        }
                    }else{
                        isError=1;
                        break;
                    }



                }



            }
            sqlLite.close();

            return isError;
        }

        @Override
        protected void onPostExecute(Integer s) {
            if(progress!=null){
                progress.dismiss();
            }
            if(s==0){
                AlertDialog.Builder builder = new AlertDialog.Builder(CheckInList.this);
                builder.setTitle("Confirmation");
                builder.setCancelable(false);
                builder.setMessage("Data has been uploaded successfully");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });

                builder.show();
            }else if(isError==1){
                showErrorDialog();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(CheckInList.this);
                builder.setTitle("Confirmation");
                builder.setCancelable(false);
                builder.setMessage("No data available for uploading");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });

                builder.show();
            }
            super.onPostExecute(s);
        }
    }
}