package survey.interspeed.com.surveyapp.model;

/**
 * Created by anupamchugh on 09/02/16.
 */
public class DataModel {

    String route;
    String routeid;
    String cluster;
    String checkinid;


    public DataModel(String route, String routeid, String cluster, String checkinid ) {
        this.route=route;
        this.routeid=routeid;
        this.cluster=cluster;
        this.checkinid=checkinid;

    }


    public String getName() {
        return route;
    }


    public String getType() {
        return routeid;
    }


    public String getVersion_number() { return cluster;}


    public String getCheckId() {
        return checkinid;
    }

}
