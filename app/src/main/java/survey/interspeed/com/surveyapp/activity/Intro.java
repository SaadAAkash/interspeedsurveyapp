package survey.interspeed.com.surveyapp.activity;

/**
 * Created by interspeed.com.bd on 2/12/2018.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import survey.interspeed.com.surveyapp.R;

public class Intro extends Activity
{
    int splashDelay = 1500;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        //set manual height and width to the logo

        ImageView logo = (ImageView) findViewById(R.id.logo);
        logo.setMaxWidth((width*2)/3);

        // Adding a Timer to exit splash screen
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run()
            {
                startActivity(new Intent(Intro.this, LoginActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }, splashDelay);


    }
}