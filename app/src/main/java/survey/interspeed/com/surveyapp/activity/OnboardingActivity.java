package survey.interspeed.com.surveyapp.activity;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import survey.interspeed.com.surveyapp.BuildConfig;
import survey.interspeed.com.surveyapp.R;
import survey.interspeed.com.surveyapp.boardcastreceiver.AlarmReceiver;
import survey.interspeed.com.surveyapp.database.SqlLite;
import survey.interspeed.com.surveyapp.service.LocationUpdatesService;

/**
 * Created by Android on 12/6/2017.
 */

public class OnboardingActivity extends BaseActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    private TextView dateTv;

    private Button btnJourney;

    private Button btnCheckIn;

    private Button btnCheckInList;

    private static final String TAG = SurveyActivity.class.getSimpleName();

    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    // The BroadcastReceiver used to listen from broadcasts from the service.
//    private LocationUpdateReceiver locationUpdateReceiver;

    // A reference to the service used to get location updates.
    //   private LocationUpdatesService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    // Monitors the state of the connection to the service.
    /*private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };*/

    private Realm realm;
    PendingIntent pendingIntent;
    SharedPreferences status;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //     locationUpdateReceiver = new LocationUpdateReceiver();
        setContentView(R.layout.activity_on_boarding);
        dateTv = (TextView) findViewById(R.id.dateTv);
        dateTv.setText(android.text.format.DateFormat.getDateFormat(getApplicationContext()).format(new Date()));

        btnJourney = (Button) findViewById(R.id.btnJourney);
        btnCheckIn = (Button) findViewById(R.id.btnCheckIn);
        btnCheckInList = (Button) findViewById(R.id.btnCheckInList);

        //ALARM RECEIVER AS PENDING INTENT

        Intent alarm = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarm, PendingIntent.FLAG_UPDATE_CURRENT);


        status = getSharedPreferences("status", 0);


        btnJourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (gpsEnabled())
                {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
                    {
                        // Do something for lollipop and above versions
                        // Check that the user hasn't revoked permissions by going to Settings.

                        if (!checkPermissions()) {
                            requestPermissions();
                        } else {
                            if (status.getBoolean("isStop", true)) {
                                btnJourney.setText(getString(R.string.stop_journey));
                                btnCheckIn.setVisibility(View.VISIBLE);
                                Calendar calendar = Calendar.getInstance();
                                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 30 * 1000, pendingIntent);
                                SharedPreferences.Editor edit = status.edit();
                                edit.putBoolean("isStop", false);
                                edit.commit();
                            } else {

                                AlertDialog.Builder builder = new AlertDialog.Builder(OnboardingActivity.this);
                                builder.setTitle("Confirmation");
                                builder.setCancelable(true);
                                builder.setMessage("Stop Journey will Discard Path Info. Are You Sure to Continue?");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        btnJourney.setText(getString(R.string.start_journey));
                                        btnCheckIn.setVisibility(View.GONE);
                                        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                        manager.cancel(pendingIntent);
                                        SharedPreferences.Editor edit = status.edit();
                                        edit.putBoolean("isStop", true);
                                        edit.commit();
                                        SqlLite db = new SqlLite(OnboardingActivity.this);
                                        db.open();
                                        db.deleteLocation();
                                        db.close();
                                    }
                                });
                                builder.setNegativeButton("Cancel", null);
                                builder.show();

                            }

                        }

                    }
                    else  //if lower version than M
                    {
                        if (status.getBoolean("isStop", true)) {
                            btnJourney.setText(getString(R.string.stop_journey));
                            btnCheckIn.setVisibility(View.VISIBLE);
                            Calendar calendar = Calendar.getInstance();
                            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() + 1000, 30 * 1000, pendingIntent);
                            SharedPreferences.Editor edit = status.edit();
                            edit.putBoolean("isStop", false);
                            edit.commit();
                        } else {

                            AlertDialog.Builder builder = new AlertDialog.Builder(OnboardingActivity.this);
                            builder.setTitle("Confirmation");
                            builder.setCancelable(true);
                            builder.setMessage("Stop Journey will Discard Path Info. Are You Sure to Continue?");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    btnJourney.setText(getString(R.string.start_journey));
                                    btnCheckIn.setVisibility(View.GONE);
                                    AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                    manager.cancel(pendingIntent);
                                    SharedPreferences.Editor edit = status.edit();
                                    edit.putBoolean("isStop", true);
                                    edit.commit();
                                    SqlLite db = new SqlLite(OnboardingActivity.this);
                                    db.open();
                                    db.deleteLocation();
                                    db.close();
                                }
                            });
                            builder.setNegativeButton("Cancel", null);
                            builder.show();

                        }
                    }
                }
            }
        });

        btnCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OnboardingActivity.this, SurveyActivity.class));
           //     finish();
            }
        });

        btnCheckInList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* ArrayList<CheckIn> checkInList = new ArrayList(realm.where(CheckIn.class).findAll());
                for (int i = 0; i < checkInList.size(); i++) {
                    Log.v("logCheck", checkInList.get(i).toString());
                }*/

                if (gpsEnabled())
                {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        // Do something for lollipop and above versions
                        // Check that the user hasn't revoked permissions by going to Settings.

                        if (!checkPermissions()) {
                            requestPermissions();
                        } else {
                            startActivity(new Intent(OnboardingActivity.this, CheckInList.class));
                        }
                    } else {
                        startActivity(new Intent(OnboardingActivity.this, CheckInList.class));
                    }
                }
                /*SqlLite sqlLite = new SqlLite(OnboardingActivity.this);
                sqlLite.open();
                String result[][] = sqlLite.getLatLon();
                String strResult = "";
                if (result != null)
                    for (int i = 0; i < result[0].length; i++) {
                        strResult = strResult + result[0][i] + "," + result[1][i] + "\n";
                    }
                Log.e("lat", strResult.toString());
                sqlLite.close();*/
            }
        });

        realm = Realm.getDefaultInstance();
    }

    private boolean gpsEnabled()
    {
        if (!((LocationManager) getSystemService(LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(OnboardingActivity.this, "Turn on GPS", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            return false;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
       /* bindService(new Intent(this, LocationUpdatesService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);*/
        /*bindService(new Intent(this, MyLocationServices.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);*/
//        startService(new Intent(this, MyLocationServices.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setButtonsState(status.getBoolean("isStop",
                true));
       /* LocalBroadcastManager.getInstance(this).registerReceiver(locationUpdateReceiver,
                new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));*/
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            //         unbindService(mServiceConnection);
            mBound = false;
        }
       /* LocalBroadcastManager.getInstance(this).unregisterReceiver(locationUpdateReceiver);*/
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(R.id.activity_onboarding),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(OnboardingActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(OnboardingActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.

            } else {
                Snackbar.make(
                        findViewById(R.id.activity_onboarding),
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        }
    }

    /**
     * Receiver for broadcasts sent by {@link LocationUpdatesService}.
     */
    private class LocationUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
            if (location != null) {
                /*Toast.makeText(OnboardingActivity.this, location.getLatitude() + " " + location.getLongitude(),
                        Toast.LENGTH_SHORT).show();*/
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.


    }

    private void setButtonsState(boolean requestingLocationUpdates) {
        if (requestingLocationUpdates) {
            btnJourney.setText(getString(R.string.start_journey));
            btnCheckIn.setVisibility(View.GONE);
        } else {

            btnJourney.setText(getString(R.string.stop_journey));
            btnCheckIn.setVisibility(View.VISIBLE);
        }
    }
}
