package survey.interspeed.com.surveyapp.model;

import java.util.List;

/**
 * Created by Android on 12/5/2017.
 */

public class Area {

    public String name;
    public List<House> houseList;

    public Area(String name, List<House> houseList) {
        this.name = name;
        this.houseList = houseList;
    }
}
