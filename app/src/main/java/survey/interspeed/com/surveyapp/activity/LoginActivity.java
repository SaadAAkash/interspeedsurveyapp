package survey.interspeed.com.surveyapp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.jsonwizard.validators.edittext.RequiredValidator;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import survey.interspeed.com.surveyapp.Config;
import survey.interspeed.com.surveyapp.R;
import survey.interspeed.com.surveyapp.model.User;

public class LoginActivity extends BaseActivity {

    private MaterialEditText bpId;    //user id
    private MaterialEditText teamId;
    private MaterialEditText password;
    private ProgressDialog pd;
    private boolean isDestroyed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (isContained(CLIENT_ID)) //isContained() is in BaseActivity
        {
            startActivity(new Intent(LoginActivity.this, OnboardingActivity.class));
            finish();
        }

        bpId = (MaterialEditText) findViewById(R.id.bpId);
        teamId = (MaterialEditText) findViewById(R.id.teamId);
        password = (MaterialEditText) findViewById(R.id.password);

        // Tr:
        bpId.setText("a001");
        teamId.setText("teama1");
        password.setText("12345");


        bpId.addValidator(new RequiredValidator(getString(R.string.bp_id_is_required)));
        teamId.addValidator(new RequiredValidator(getString(R.string.team_id_is_required)));
        password.addValidator(new RequiredValidator(getString(R.string.password_is_required)));



        ((Button) findViewById(R.id.btnContinue)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bpId.validate() && teamId.validate() && password.validate()) {
                    logginIn();
                }
            }
        });
    }

    private void logginIn() {
        pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.loading));
        pd.show();

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("cid", bpId.getText().toString())
                .add("sid", teamId.getText().toString())
                .add("pass", password.getText().toString())
                .build();
        final Request request = new Request.Builder()
                .url(Config.URL + "/login.php")
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                pd.dismiss();

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    if (!isDestroyed) {
                        pd.dismiss();
                        try {
                            JSONArray jsonArray = new JSONArray(response.body().string());
                            //making jsonarray from php response

                            User user = new Gson().fromJson(jsonArray.getJSONObject(0).toString(), User.class);
                            saveValue(CLIENT_ID, user.getClientId());
                            //save user info to sharedpref

                            LoginActivity.this.startActivity(new Intent(LoginActivity.this, OnboardingActivity.class));
                            LoginActivity.this.finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                SharedPreferences status = getSharedPreferences("status",0);
                SharedPreferences.Editor edit = status.edit();
                edit.putString("cid", bpId.getText().toString());
                edit.putString("sid", teamId.getText().toString());
                edit.commit();


            }
        });
    }

    @Override
    protected void onDestroy() {
        isDestroyed = true;
        super.onDestroy();
    }
}
