package survey.interspeed.com.surveyapp.model.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Android on 12/7/2017.
 */

public class CheckIn extends RealmObject {

    @PrimaryKey
    private String id;

    @Required
    private String clientId;

    @Required
    private Date date;

    private double lat;

    private double lng;

    @Required
    private String region;

    @Required
    private String area;

    @Required
    private String house;

    @Required
    private String point;

    @Required
    private String route;

    @Required
    private String routeId;

    @Required
    private String cluster;

    private boolean isUploaded;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setUploaded(boolean uploaded) {
        isUploaded = uploaded;
    }

    @Override
    public String toString() {
        return "CheckIn{" +
                "id=" + id +
                ", clientId='" + clientId + '\'' +
                ", date=" + date +
                ", lat=" + lat +
                ", lng=" + lng +
                ", region='" + region + '\'' +
                ", area='" + area + '\'' +
                ", house='" + house + '\'' +
                ", point='" + point + '\'' +
                ", route='" + route + '\'' +
                ", routeId='" + routeId + '\'' +
                ", cluster='" + cluster + '\'' +
                ", isUploaded=" + isUploaded +
                '}';
    }
}
