package survey.interspeed.com.surveyapp.model;

import java.util.List;

/**
 * Created by Android on 12/5/2017.
 */

public class Point {
    public String name;
    public List<Route> routeList;

    public Point(String name, List<Route> routeList) {
        this.name = name;
        this.routeList = routeList;
    }
}
