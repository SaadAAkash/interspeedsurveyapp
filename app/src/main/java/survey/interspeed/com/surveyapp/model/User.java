package survey.interspeed.com.surveyapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("client_id")
    @Expose
    private String clientId;
    @SerializedName("s_id")
    @Expose
    private String sId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("asset_select")
    @Expose
    private String assetSelect;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cetegory")
    @Expose
    private String cetegory;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("contact_person")
    @Expose
    private String contactPerson;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("last_servey_date")
    @Expose
    private String lastServeyDate;
    @SerializedName("next_survey_due_date")
    @Expose
    private String nextSurveyDueDate;
    @SerializedName("asset_manager")
    @Expose
    private String assetManager;
    @SerializedName("description")
    @Expose
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSId() {
        return sId;
    }

    public void setSId(String sId) {
        this.sId = sId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAssetSelect() {
        return assetSelect;
    }

    public void setAssetSelect(String assetSelect) {
        this.assetSelect = assetSelect;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCetegory() {
        return cetegory;
    }

    public void setCetegory(String cetegory) {
        this.cetegory = cetegory;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLastServeyDate() {
        return lastServeyDate;
    }

    public void setLastServeyDate(String lastServeyDate) {
        this.lastServeyDate = lastServeyDate;
    }

    public String getNextSurveyDueDate() {
        return nextSurveyDueDate;
    }

    public void setNextSurveyDueDate(String nextSurveyDueDate) {
        this.nextSurveyDueDate = nextSurveyDueDate;
    }

    public String getAssetManager() {
        return assetManager;
    }

    public void setAssetManager(String assetManager) {
        this.assetManager = assetManager;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}