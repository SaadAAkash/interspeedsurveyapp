package survey.interspeed.com.surveyapp.model;

import java.util.List;

/**
 * Created by Android on 12/5/2017.
 */

public class Route {
    public String name;
    public List<RouteId> routeIdList;

    public Route(String name, List<RouteId> routeIdList) {
        this.name = name;
        this.routeIdList = routeIdList;
    }
}
