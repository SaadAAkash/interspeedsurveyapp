package survey.interspeed.com.surveyapp.model;

import java.util.List;

/**
 * Created by Android on 12/5/2017.
 */

public class RouteId {
    public String name;
    public List<String> clusterList;

    public RouteId(String name, List<String> clusterList) {
        this.name = name;
        this.clusterList = clusterList;
    }
}
