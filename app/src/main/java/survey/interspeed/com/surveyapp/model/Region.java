package survey.interspeed.com.surveyapp.model;

import java.util.List;

/**
 * Created by Android on 12/5/2017.
 */

public class Region {
    public String name;
    public List<Area> areaList;

    public Region(String name, List<Area> areaList) {
        this.name = name;
        this.areaList = areaList;
    }
}
